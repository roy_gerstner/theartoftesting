# TheArtOfTesting

[![pipeline status](https://gitlab.com/roy_gerstner/theartoftesting/badges/master/pipeline.svg)](https://gitlab.com/roy_gerstner/theartoftesting/-/commits/master)

[![Coverage report NEW](https://gitlab.com/gitlab-org/gitlab-ce/badges/master/coverage.svg?job=gtest)](https://roy_gerstner.gitlab.io/theartoftesting)

[![Coverage report](https://gitlab.com/gitlab-org/gitlab-ce/badges/master/coverage.svg?job=gtest)](https://roy_gerstner.gitlab.io/theartoftesting)

[Book: The art of Software testing](https://www.amazon.com/-/de/dp/B005PETXRM/ref=sr_1_3?__mk_de_DE=%C3%85M%C3%85%C5%BD%C3%95%C3%91&dchild=1&keywords=art+of+testing&qid=1587193916&sr=8-3)

[Tutorial:Software Testing Tutorial](https://artoftesting.com/software-testing-tutorial)

## CMake and VsCode integration:

    https://code.visualstudio.com/docs/cpp/cmake-linux
    
## Using gcc/g++ instead of clang in OSX

    https://stackoverflow.com/questions/64870870build-error-with-make-for-c-code-in-osx

### Using gcov/lcov with cmake

    https://jhbell.com/using-cmake-and-gcov

    Linux man-pages: 
        https://linux.die.net/man/1/gcov
        https://linux.die.net/man/1/lcov
    

    CMakeLists.txt changes:
        add Coverage option to compiler and linker for your SUT
        
            add_library(
                dreieck
                src/Dreieck.cpp
                )
            set_target_properties(dreieck PROPERTIES
                CXX_STANDARD 11
                CXX_STANDARD_REQUIRED ON
                CXX_EXTENSIONS ON
                COMPILE_OPTIONS -coverage
                LINK_OPTIONS "--coverage -lgcov"
            )

        add gcov lib to linker
            
            target_link_libraries(
                unit_tests
                googletest
                dreieck
                pthread
                gcov
            )


**Badges**

    Todo: Add badges for Test-Status and for Code-Coverage

    https://docs.gitlab.com/ee/user/project/badges.html
    https://docs.gitlab.com/ee/ci/testing/code_coverage.html#add-code-coverage-results-to-merge-requests
    https://www.patricksoftwareblog.com/add_badges_to_a_gitlab_project.html

    Hint:
     Test-Status can bee seen in page: https://gitlab.com/roy_gerstner/theartoftesting/-/pipelines/1589047099/test_report
     Current Coverage Report is linked in the Badge
   
   Tbc: Wie wird der richtige status für Google-Test und Coverage ermittelt?
    

**googletest**

    https://github.com/google/googletest
    
**googletest and cmake integration**
    https://github.com/gocarlos/gtest-demo-gitlab

**googletest and github badges**
    tbc: Wie bekomme ich die Ergebnisse von googletest in die Badges von gitlab?



**Importing a CMake Projekt into Eclipse CDT (tbd)**

    https://stackoverflow.com/questions/11645575/importing-a-cmake-project-into-eclipse-cdt

**Using googletest in Eclipse (tbd)**

    https://stackoverflow.com/questions/3951808/using-googletest-in-eclipse-how

**googletest for eclipse Tutorial (tbd)**

    http://correderajorge.github.io/Google-Test-Tutorial/

**ThreadSanitizer, Helgrind, Valgrid, DRD Integration (tbd)**

    https://simplifycpp.org/?id=a0557
    https://www.youtube.com/watch?v=KhcbRC8LDvo

