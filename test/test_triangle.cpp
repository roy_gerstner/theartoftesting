#include "gtest/gtest.h"
#include "Dreieck.h"

TEST(triangle, correctTriangle)
{   
    int a,b,c;
    DreieckTyp res;

    a = 2;
    b = 3;
    c = 4;
    
    res = getType(a,b,c);
    // EXPECT_EQ <> Assert_EQ
    EXPECT_EQ(res, DreieckTyp::Dreieck) << "Expect triangle";

    a = 4;
    b = 3;
    c = 2;

    res = getType(a,b,c);
    EXPECT_EQ(res, DreieckTyp::Dreieck) << "Expect triangle";

    a = 15;
    b = 26;
    c = 20;

    res = getType(a,b,c);
    EXPECT_EQ(res, DreieckTyp::Dreieck) << "Expect triangle";
}

TEST(triangle, correctTriangleGleichschenklig)
{
    int a,b,c;
    DreieckTyp res;

    a = 2;
    b = 3;
    c = 2;
    res = getType(a,b,c);
    EXPECT_EQ(res, DreieckTyp::GleichschenkligesDreieck);
    
    a = 1;
    b = 2;
    c = 2;
    res = getType(a,b,c);
    EXPECT_EQ(res, DreieckTyp::GleichschenkligesDreieck);
    
    a = 2;
    b = 2;
    c = 1;
    res = getType(a,b,c);
    EXPECT_EQ(res, DreieckTyp::GleichschenkligesDreieck);
}

TEST(triangle, correctTriangleGleichseitiges)
{
    int a,b,c;
    DreieckTyp res;

    a = 2;
    b = 2;
    c = 2;
    res = getType(a,b,c);
    EXPECT_EQ(res, DreieckTyp::GleichseitigesDreieck);
    
    a = 1;
    b = 1;
    c = 1;
    res = getType(a,b,c);
    EXPECT_EQ(res, DreieckTyp::GleichseitigesDreieck);
    
    a = 100;
    b = 100;
    c = 100;
    res = getType(a,b,c);
    EXPECT_EQ(res, DreieckTyp::GleichseitigesDreieck);
}


TEST(triangle, invalidTriangle)
{   
    int a,b,c;
    DreieckTyp res;

    a = 0;
    b = 0;
    c = 0;
    
    res = getType(a,b,c);
    EXPECT_EQ(res, DreieckTyp::KeinDreieck);

    a = 3;
    b = 4;
    c = 0;

    res = getType(a,b,c);
    EXPECT_EQ(res, DreieckTyp::KeinDreieck);

    a = 0;
    b = 4;
    c = 3;

    res = getType(a,b,c);
    EXPECT_EQ(res, DreieckTyp::KeinDreieck);

    a = 3;
    b = 0;
    c = 4;

    res = getType(a,b,c);
    EXPECT_EQ(res, DreieckTyp::KeinDreieck);

    a = 4;
    b = 4;
    c = 0;

    res = getType(a,b,c);
    EXPECT_EQ(res, DreieckTyp::KeinDreieck);

    a = 0;
    b = 4;
    c = 4;

    res = getType(a,b,c);
    EXPECT_EQ(res, DreieckTyp::KeinDreieck);

    a = 4;
    b = 0;
    c = 4;

    res = getType(a,b,c);
    EXPECT_EQ(res, DreieckTyp::KeinDreieck);

    a = -1;
    b = -1;
    c = -1;

    res = getType(a,b,c);
    EXPECT_EQ(res, DreieckTyp::KeinDreieck);

    a = -3;
    b = 4;
    c = 2;

    res = getType(a,b,c);
    EXPECT_EQ(res, DreieckTyp::KeinDreieck);

    a = 3;
    b = -4;
    c = 2;

    res = getType(a,b,c);
    EXPECT_EQ(res, DreieckTyp::KeinDreieck);

    a = 3;
    b = 4;
    c = -2;

    res = getType(a,b,c);
    EXPECT_EQ(res, DreieckTyp::KeinDreieck);

    
    a = 5;
    b = 8;
    c = 100;

    res = getType(a,b,c);
    EXPECT_EQ(res, DreieckTyp::KeinDreieck);
}
