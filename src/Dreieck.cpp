#include "Dreieck.h"

DreieckTyp validateDreieck(int a, int b, int c)
{
    DreieckTyp myFirstResult = DreieckTyp::KeinDreieck;
    DreieckTyp mySecondResult = DreieckTyp::KeinDreieck;

    if( c >= (a+b) || b >= (a+c) || a >= (b+c) )
    {
        myFirstResult = DreieckTyp::KeinDreieck;
    }
    else
    {
        myFirstResult = DreieckTyp::Dreieck;
    }

/*
    if( (a + b - c) == 0 ) // Doesn't work aber wieso kommt gcov hier nicht rein?
    {
        mySecondResult = DreieckTyp::KeinDreieck;
    }
    else
    {
        mySecondResult = DreieckTyp::Dreieck;
    }

    if(myFirstResult != mySecondResult)
    {
        return DreieckTyp::InvalidCompare;
    }
*/

    return myFirstResult;
}

DreieckTyp validateGleichseitigesDreieck(int a, int b, int c)
{
    DreieckTyp myResult;
    if( a == b && a == c)
    {
        myResult = DreieckTyp::GleichseitigesDreieck;
    }
    else
    {
        myResult = DreieckTyp::Dreieck;
    }
    return myResult;
}

DreieckTyp validateGleichschenkligesDreieck(int a, int b, int c)
{
    DreieckTyp myResult;
    if( a == b || a == c || b==c)
    {
        myResult = DreieckTyp::GleichschenkligesDreieck;
    }
    else
    {
        myResult = DreieckTyp::Dreieck;
    }
    return myResult;
}

DreieckTyp checkInvalidParameters(int a, int b, int c)
{
    DreieckTyp myResult = DreieckTyp::InvalidParameter;

    if( a < 1 || b < 1 || c < 1)
    {
        myResult = DreieckTyp::InvalidParameter;
    }
    else
    {
        myResult = DreieckTyp::KeinDreieck;
    }
    return myResult;
}

DreieckTyp getType(int a, int b, int c)
{ 
    DreieckTyp testResult = DreieckTyp::KeinDreieck;

    testResult = checkInvalidParameters(a, b, c);

    if( testResult != DreieckTyp::InvalidParameter)
    {
        testResult = validateDreieck(a, b ,c);
        
        if(testResult == DreieckTyp::Dreieck)
        {
            testResult = validateGleichseitigesDreieck(a, b, c);
            if(testResult == DreieckTyp::Dreieck )
            {
                testResult = validateGleichschenkligesDreieck(a, b, c);
            }
        }
    }
    else
    {
        testResult = DreieckTyp::KeinDreieck;
    }
    return testResult;
}
