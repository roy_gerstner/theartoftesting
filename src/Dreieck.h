#include <stdint.h>

enum class DreieckTyp : uint16_t
    {
        InvalidParameter = 0,
        InvalidCompare,
        KeinDreieck,
        Dreieck,
        GleichschenkligesDreieck,
        GleichseitigesDreieck
    };

DreieckTyp getType(int, int, int);
