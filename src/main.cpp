#include <iostream>
#include <stdint.h>
#include "Dreieck.h"

int main()
{   
    int a,b,c;
    std::cout << "Hello Test!" << std::endl;
    std::cout << " Definition Dreieck: Ein Dreieck wird durch drei Punkte definiert, die nicht auf einer Geraden liegen." << std::endl;


    a = 2;
    b = 3;
    c = 2;
    std::cout << "Testing(" << a << ", " << b << ", " <<  c << ") : Typ = " << static_cast<int>(getType(a,b,c)) << std::endl;
    
    a = 1;
    b = 2;
    c = 2;
    std::cout << "Testing(" << a << ", " << b << ", " <<  c << ") : Typ = " << static_cast<int>(getType(a,b,c)) << std::endl;
    
    a = 2;
    b = 2;
    c = 2;
    std::cout << "Testing(" << a << ", " << b << ", " <<  c << ") : Typ = " << static_cast<int>(getType(a,b,c)) << std::endl;

    a = 1;
    b = 1;
    c = 3;
    std::cout << "Testing(" << a << ", " << b << ", " <<  c << ") : Typ = " << static_cast<int>(getType(a,b,c)) << std::endl;

    a = 3;
    b = 4;
    c = 5;
    std::cout << "Testing(" << a << ", " << b << ", " <<  c << ") : Typ = " << static_cast<int>(getType(a,b,c)) << std::endl;

    a = 0;
    b = 0;
    c = 0;
    std::cout << "Testing(" << a << ", " << b << ", " <<  c << ") : Typ = " << static_cast<int>(getType(a,b,c)) << std::endl;

    a = 3;
    b = 4;
    c = 0;
    std::cout << "Testing(" << a << ", " << b << ", " <<  c << ") : Typ = " << static_cast<int>(getType(a,b,c)) << std::endl;

    a = 4;
    b = 4;
    c = 0;
    std::cout << "Testing(" << a << ", " << b << ", " <<  c << ") : Typ = " << static_cast<int>(getType(a,b,c)) << std::endl;

    a = -1;
    b = -1;
    c = -1;
    std::cout << "Testing(" << a << ", " << b << ", " <<  c << ") : Typ = " << static_cast<int>(getType(a,b,c)) << std::endl;

    a = -3;
    b = 4;
    c = 2;
    std::cout << "Testing(" << a << ", " << b << ", " <<  c << ") : Typ = " << static_cast<int>(getType(a,b,c)) << std::endl;

    a = 15;
    b = 26;
    c = 20;
    std::cout << "Testing(" << a << ", " << b << ", " <<  c << ") : Typ = " << static_cast<int>(getType(a,b,c)) << std::endl;

    a = -2;
    b = -3;
    c = -4;
    std::cout << "Testing(" << a << ", " << b << ", " <<  c << ") : Typ = " << static_cast<int>(getType(a,b,c)) << std::endl;

    a = 5;
    b = 8;
    c = 13;
    std::cout << "Testing(" << a << ", " << b << ", " <<  c << ") : Typ = " << static_cast<int>(getType(a,b,c)) << std::endl;
    return 0;
}
