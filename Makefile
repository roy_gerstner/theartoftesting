.PHONY: all cov clean build-centos6  run-centos6

GCC=g++
INCLUDE=-I./include -I./
# interessante Optionen für die Zukunft
# siehe auch https://linux.die.net/man/1/g++
# -fcheck-new -fno-implement-inlines  
# -Wold-style-cast
# -fcond-mismatch
# -fsigned-bitfields -fsigned-char 
# -funsigned-bitfields -funsigned-char
# -Wunreachable-code

CFLAGS=-c -std=c++11 -Wall $(INCLUDE) 
CovFLAGS=--coverage -fprofile-arcs -ftest-coverage -pedantic -O0

all: DreieckType

DreieckType: src/main.o src/Dreieck.o
	g++ --version
	$(GCC) -std=c++11 -Wall -O0 -g -o DreieckType src/*.o

cov:
	$(GCC) $(CFLAGS) $(CovFLAGS) src/main.cpp -o cov/main.o  $(INCLUDE)
	$(GCC) $(CFLAGS) $(CovFLAGS) src/Dreieck.cpp -o cov/Dreieck.o $(INCLUDE)
	$(GCC) $(LDFLAGS) $(CovFLAGS) -o cov/DreieckType_cov cov/*.o

cov_execute: cov/DreieckType_cov
	cov/DreieckType_cov
	gcov src/Dreieck.cpp
	lcov --directory . --capture --output-file coverage.info
	lcov --remove coverage.info '/usr/*' --output-file coverage.info
	lcov --list coverage.info
	genhtml -o public coverage.info
clean:
	rm -f *.exe src/*.o cov/*.*

src/main.o : src/main.cpp
	$(GCC) $(CFLAGS) src/main.cpp -o src/main.o  $(INCLUDE)

src/Dreieck.o : src/Dreieck.cpp
	$(GCC) $(CFLAGS) src/Dreieck.cpp -o src/Dreieck.o $(INCLUDE)



build-centos6:
	docker build  -t hello-centos6 -f docker/centos6/Dockerfile .

run-centos6: build-centos6
	docker run -ti --rm hello-centos6
